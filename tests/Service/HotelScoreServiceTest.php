<?php

namespace App\Tests\Service;

use App\Repository\Model\ReviewModel;
use App\Repository\ReviewRepository;
use App\Service\HotelScoreService;
use PHPUnit\Framework\TestCase;

class HotelScoreServiceTest extends TestCase
{
    /**
     * @var ReviewRepository
     */
    private $reviewRepository;

    protected function setUp(): void
    {
        $this->reviewRepository = $this->createMock(ReviewRepository::class);
    }

    public function testGetScoreNoResults(): void
    {
        $this->reviewRepository
            ->method('findReviews')
            ->willReturn([]);

        $hotelScoreService = new HotelScoreService($this->reviewRepository);

        $groupedScore = $hotelScoreService->findReviews(1, 1595824755, 1608824755);
        $this->assertEquals(0, $groupedScore->getCount());
        $this->assertEquals(0, $groupedScore->getAverage());
        $this->assertEquals([], $groupedScore->getReviewsCollectionModel());

        $groupedScore = $hotelScoreService->findReviews(1, 0, -1);
        $this->assertEquals([], $groupedScore->getReviewsCollectionModel());
    }

    public function testGetScore(): void
    {
        $this->reviewRepository
            ->method('findReviews')
            ->willReturn($this->provideReviews());

        $hotelScoreService = new HotelScoreService($this->reviewRepository);

        $groupedMonthly = $hotelScoreService->findReviews(1, 1595824755, 1608824755);
        $this->assertCount(5, $groupedMonthly->getReviewsCollectionModel());
        $this->assertCount(1, $groupedMonthly->getReviewsCollectionModel()['August, 2020']->getReviews());
        $this->assertEquals(3.43, $groupedMonthly->getAverage());
        $this->assertEquals(7, $groupedMonthly->getCount());

        $groupedWeekly = $hotelScoreService->findReviews(1, 1595824755, 1598824755);
        $this->assertCount(5, $groupedWeekly->getReviewsCollectionModel());
        $this->assertCount(1, $groupedWeekly->getReviewsCollectionModel()['August, 2020(35)']->getReviews());
        $this->assertEquals(3.43, $groupedWeekly->getAverage());

        $groupedDaily = $hotelScoreService->findReviews(1, 1595824755, 1597824755);
        $this->assertCount(7, $groupedDaily->getReviewsCollectionModel());
        $this->assertCount(1, $groupedDaily->getReviewsCollectionModel()['September 28, 2020']->getReviews());
        $this->assertEquals(3.43, $groupedDaily->getAverage());
    }

    /**
     * @return ReviewModel[]
     */
    private function provideReviews(): array
    {
        return [
            new ReviewModel(1, 1, 5, 'Test comment', '2020-07-28 10:25:55'),
            new ReviewModel(2, 1, 3, 'Test comment', '2020-07-29 10:25:55'),
            new ReviewModel(3, 1, 2, 'Test comment', '2020-07-30 10:25:55'),
            new ReviewModel(4, 1, 4, 'Test comment', '2020-08-28 10:25:55'),
            new ReviewModel(5, 1, 1, 'Test comment', '2020-09-28 10:25:55'),
            new ReviewModel(6, 1, 4, 'Test comment', '2020-10-28 10:25:55'),
            new ReviewModel(7, 1, 5, 'Test comment', '2020-12-28 10:25:55'),
        ];
    }
}
