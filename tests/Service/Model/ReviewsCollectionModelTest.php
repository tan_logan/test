<?php

namespace App\Tests\Service\Model;

use App\Repository\Model\ReviewModel;
use App\Service\Model\ReviewsCollectionModel;
use PHPUnit\Framework\TestCase;

class ReviewsCollectionModelTest extends TestCase
{
    public function testModelWithDifferentData(): void
    {
        $reviewsCollectionModel = new ReviewsCollectionModel();
        $this->assertEquals(0, $reviewsCollectionModel->getCount());
        $this->assertEquals(0, $reviewsCollectionModel->getAverage());
        $this->assertEquals([], $reviewsCollectionModel->getReviews());
        $this->assertEquals('{"average-score":0,"review-count":0,"reviews":[]}', json_encode($reviewsCollectionModel));

        $reviewsCollectionModel->addReview(new ReviewModel(1, 1, 5, 'test', '2020-11-04 22:19:15'));
        $reviewsCollectionModel->addReview(new ReviewModel(2, 1, 4, 'test', '2020-11-04 22:19:15'));
        $reviewsCollectionModel->addReview(new ReviewModel(3, 1, 2, 'test', '2020-11-04 22:19:15'));

        $this->assertEquals(3, $reviewsCollectionModel->getCount());
        $this->assertEquals(3.67, $reviewsCollectionModel->getAverage());
        $this->assertCount(3, $reviewsCollectionModel->getReviews());
        $this->assertEquals(4, $reviewsCollectionModel->getReviews()[1]->getScore());
    }
}
