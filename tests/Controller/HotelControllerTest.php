<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HotelControllerTest extends WebTestCase
{
    public function testScoreNotFound()
    {
        $client = static::createClient();

        $client->request('GET', '/hotel/findReviews/3/1595824755/1497824755');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals(
            '{"average-score":0,"review-count":0,"date-group":[]}',
            $client->getResponse()->getContent()
        );
    }

    public function testScore()
    {
        $client = static::createClient();

        $client->request('GET', '/hotel/findReviews/3/1595824755/1597824755');

        $responseContent = json_decode($client->getResponse()->getContent(), true);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertNotEmpty($responseContent['date-group']);
        $this->assertTrue($responseContent['review-count'] > 0);
    }
}
