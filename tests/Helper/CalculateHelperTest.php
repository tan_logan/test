<?php

namespace App\Tests\Helper;

use App\Helper\CalculateHelper;
use PHPUnit\Framework\TestCase;

class CalculateHelperTest extends TestCase
{
    public function testAverage(): void
    {
        $this->assertEquals(11.17, CalculateHelper::average(134, 12));
        $this->assertEquals(47.96, CalculateHelper::average(5899, 123));
    }
}
