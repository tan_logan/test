<?php

namespace App\Repository\Model;

use JsonSerializable;

class ReviewModel implements JsonSerializable
{
    /** @var int */
    private $id;

    /** @var int */
    private $hotelId;

    /** @var int */
    private $score;

    /** @var string */
    private $comment;

    /** @var string */
    private $createdDate;

    /**
     * ReviewModel constructor.
     * @param int $id
     * @param int $hotelId
     * @param int $score
     * @param string $comment
     * @param string $createdDate
     */
    public function __construct(int $id, int $hotelId, int $score, string $comment, string $createdDate)
    {
        $this->id = $id;
        $this->hotelId = $hotelId;
        $this->score = $score;
        $this->comment = $comment;
        $this->createdDate = $createdDate;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getHotelId(): int
    {
        return $this->hotelId;
    }

    /**
     * @return int
     */
    public function getScore(): int
    {
        return $this->score;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @return string
     */
    public function getCreatedDate(): string
    {
        return $this->createdDate;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'hotel_id' => $this->getHotelId(),
            'created_date' => $this->getCreatedDate(),
            'score' => $this->getScore(),
            'comment' => $this->getComment(),
        ];
    }
}
