<?php

namespace App\Repository;

use App\Entity\Review;
use App\Repository\Model\ReviewModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Review|null find($id, $lockMode = null, $lockVersion = null)
 * @method Review|null findOneBy(array $criteria, array $orderBy = null)
 * @method Review[]    findAll()
 * @method Review[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReviewRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Review::class);
    }

    /**
     * @param int $hotelId
     * @param int $startDate
     * @param int $endDate
     *
     * @return ReviewModel[]
     */
    public function findReviews(int $hotelId, int $startDate, int $endDate): array
    {
        $result = $this->createQueryBuilder('r')
            ->andWhere('r.hotel_id = :hotel_id')
            ->andWhere('r.created_date > :start_date')
            ->andWhere('r.created_date < :end_date')
            ->setParameter('hotel_id', $hotelId)
            ->setParameter('start_date', date('Y-m-d H:i:s', $startDate))
            ->setParameter('end_date', date('Y-m-d H:i:s', $endDate))
            ->orderBy('r.created_date', 'ASC')
            ->getQuery()
            ->getResult();

        return $this->mapToDto($result);
    }

    /**
     * @param Review[] $result
     * @return ReviewModel[]
     */
    private function mapToDto(array $result): array
    {
        return array_map(function (Review $review) {
            return new ReviewModel(
                $review->getId(),
                $review->getHotelId(),
                $review->getScore(),
                $review->getComment() ?? '',
                $review->getCreatedDate()->format('Y-m-d H:i:s')
            );
        }, $result);
    }
}
