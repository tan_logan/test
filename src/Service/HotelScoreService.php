<?php

namespace App\Service;

use App\Helper\CalculateHelper;
use App\Repository\ReviewRepository;
use App\Service\Model\ReviewsCollectionModel;
use App\Service\Model\ReviewsResponseModel;

class HotelScoreService
{
    private const DAY_FORMAT = 'F j, Y';
    private const WEEK_FORMAT = 'F, Y(W)';
    private const MONTH_FORMAT = 'F, Y';
    private const DAY = 24 * 60 * 60;

    /**
     * @var ReviewRepository
     */
    private $reviewRepository;

    public function __construct(ReviewRepository $reviewRepository)
    {
        $this->reviewRepository = $reviewRepository;
    }

    /**
     * Return reviews for hotel grouped by date range.
     * 1 - 29 days: Grouped daily
     * 30 - 89 days: Grouped weekly
     * More than 89 days: Grouped monthly
     * Return model with no reviews if nothing found.
     *
     * @param int $hotelId
     * @param int $startDate
     * @param int $endDate
     * @return ReviewsResponseModel
     */
    public function findReviews(int $hotelId, int $startDate, int $endDate): ReviewsResponseModel
    {
        $reviews = $this->reviewRepository->findReviews($hotelId, $startDate, $endDate);
        if (count($reviews) === 0) {
            return new ReviewsResponseModel([], 0, 0);
        }

        $dateGroup = [];
        $sumScore = 0;
        $format = $this->getGroupFormat($startDate, $endDate);
        foreach ($reviews as $reviewModel) {
            $group = date($format, strtotime($reviewModel->getCreatedDate()));
            if (!isset($dateGroup[$group])) {
                $dateGroup[$group] = new ReviewsCollectionModel();
            }
            $dateGroup[$group]->addReview($reviewModel);
            $sumScore += $reviewModel->getScore();
        }

        return new ReviewsResponseModel(
            $dateGroup,
            count($reviews),
            CalculateHelper::average($sumScore, count($reviews))
        );
    }

    private function getGroupFormat(int $startDate, int $endDate): string
    {
        $days = ($endDate - $startDate) / self::DAY;

        if ($days < 30) {
            return self::DAY_FORMAT;
        }

        if ($days < 90) {
            return self::WEEK_FORMAT;
        }

        return self::MONTH_FORMAT;
    }
}
