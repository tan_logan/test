<?php

namespace App\Service\Model;

use JsonSerializable;

class ReviewsResponseModel implements JsonSerializable
{
    /** @var ReviewsCollectionModel[] */
    private $reviewsCollectionModel;

    /** @var int */
    private $count;

    /** @var float */
    private $average;

    /**
     * ScoreResponseModel constructor.
     * @param ReviewsCollectionModel[] $reviewsCollectionModel
     * @param int $count
     * @param float $average
     */
    public function __construct(array $reviewsCollectionModel, int $count, float $average)
    {
        $this->reviewsCollectionModel = $reviewsCollectionModel;
        $this->count = $count;
        $this->average = $average;
    }

    /**
     * @return ReviewsCollectionModel[]
     */
    public function getReviewsCollectionModel(): array
    {
        return $this->reviewsCollectionModel;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return float
     */
    public function getAverage(): float
    {
        return $this->average;
    }

    public function jsonSerialize(): array
    {
        return [
            'average-score' => $this->getAverage(),
            'review-count' => $this->getCount(),
            'date-group' => $this->getReviewsCollectionModel(),
        ];
    }
}
