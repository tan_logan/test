<?php

namespace App\Service\Model;

use App\Helper\CalculateHelper;
use App\Repository\Model\ReviewModel;
use JsonSerializable;

class ReviewsCollectionModel implements JsonSerializable
{
    /** @var ReviewModel[] */
    private $reviews = [];

    public function addReview(ReviewModel $review): void
    {
        $this->reviews[] = $review;
    }

    /**
     * @return ReviewModel[]
     */
    public function getReviews(): array
    {
        return $this->reviews;
    }

    public function getCount(): int
    {
        return count($this->reviews);
    }

    public function getAverage(): float
    {
        if (!$this->reviews) {
            return 0;
        }

        $sumScore = array_reduce($this->reviews, function ($carry, ReviewModel $review) {
            $carry += $review->getScore();
            return $carry;
        });

        return CalculateHelper::average($sumScore, count($this->reviews));
    }


    public function jsonSerialize()
    {
        return [
            'average-score' => $this->getAverage(),
            'review-count' => $this->getCount(),
            'reviews' => $this->getReviews(),
        ];
    }
}
