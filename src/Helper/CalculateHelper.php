<?php

namespace App\Helper;

class CalculateHelper
{
    public static function average(int $sum, int $count): float
    {
        return round(($sum / $count), 2);
    }
}
