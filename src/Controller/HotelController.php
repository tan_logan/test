<?php

namespace App\Controller;

use App\Repository\Model\ReviewModel;
use App\Service\HotelScoreService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HotelController extends AbstractController
{
    /**
     * @var HotelScoreService
     */
    private $hotelScoreService;

    public function __construct(HotelScoreService $hotelScoreService)
    {
        $this->hotelScoreService = $hotelScoreService;
    }

    /**
     * @Route("/hotel/findReviews/{hotelId<\d+>}/{startDate<\d+>}/{endDate<\d+>}", name="hotel_find_reviews")
     */
    public function findReviews(int $hotelId, int $startDate, int $endDate): Response
    {
        return $this->json($this->hotelScoreService->findReviews($hotelId, $startDate, $endDate));
    }
}
