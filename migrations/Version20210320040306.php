<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210320040306 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        for ($i = 1; $i <= 10; $i++) {
            $this->addSql('INSERT INTO hotel (name) VALUES ("Best Hotel #' . $i . '")');
        }

        for ($i = 1; $i <= 100000; $i++) {
            $hotelId = rand(1, 10);
            $score = rand(1, 5);
            $comment = 'Test comment #' . $i;
            //within two years random
            $dateCreated = date('Y-m-d H:i:s', 1553190619 + (rand(1000, 1616349019 - 1553190619)));
            $this->addSql(
                'INSERT INTO review (hotel_id, score, comment, created_date) ' .
                'VALUES ("' . $hotelId . '", "' . $score . '", "' . $comment . '" , "' . $dateCreated . '")'
            );
        }

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
